#include <iostream>
#include "Core.h"

// Entities
#include "EntityFactory.h"
#include "Background.h"
#include "Character.h"
#include "Enemy.h"
#include "Obstacle.h"
#include "Physics.h"
#include "EntityMediator.h"

// Application
#include "Window.h"
#include "MainApplication.h"

int main() {
	// Create Entities
	EntityMediator entityMediator = EntityMediator();
	EntityFactory entityFactory = EntityFactory();
	entityFactory.SetMediator(entityMediator);
	entityFactory.CreateEntity<Physics>(NULL);

	// TODO: Move this to some world generation class
	Background::Input backgroundInput{ 0.0f, 0.0f, 2.0f, 2.0f, "res/images/caveBackground.png" };
	entityFactory.CreateEntity<Background>((void*)& backgroundInput);

	Obstacle::Input crateInput1{ -0.875f, 0.0f, 0.25f, 0.25f, "res/images/crate.png" };
	entityFactory.CreateEntity<Obstacle>((void*)& crateInput1);

	Obstacle::Input crateInput2{ 0.0f, 0.5f, 0.25f, 0.25f, "res/images/crate.png" };
	entityFactory.CreateEntity<Obstacle>((void*)& crateInput2);

	Enemy::Input skeletonInput( 0.5f, 0.5f, 0.15f, 0.15f, "res/images/skeleton.png", 5 );
	entityFactory.CreateEntity<Enemy>((void*)& skeletonInput);

	Enemy::Input skeletonInput2( 0.5f, -0.5f, 0.15f, 0.15f, "res/images/skeleton.png", 5 );
	entityFactory.CreateEntity<Enemy>((void*)& skeletonInput2);

	Character::Input characterInput{ 0.0f, 0.0f, 0.25f, 0.25f };
	entityFactory.CreateEntity<Character>((void*)& characterInput);

	// Create main application
	MainApplication* application = MainApplication::GetSingleton();
	application->SetEntityMediator(&entityMediator);
	application->Initialize();

	// Run Application
	application->Run();
	glfwTerminate();
	return 0;
}
#pragma once
#include <vector>
#include "Application.h"

class EntityFactory;
class WindowData;
class Enemy;

class Engagement : public Application
{
public:
    Engagement(const WindowData& windowData, Enemy* enemy);
    ~Engagement();
    bool OnEvent(Event& e);
    void Initialize();
    void Update();
    bool OnKeyPressed(Event& e);

private:
    Enemy* m_Enemy;
    EntityFactory* m_EntityFactory;
    std::vector<unsigned int> m_HeartIDs;
};
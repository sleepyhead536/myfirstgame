#include <iostream>
#include <functional>
#include "Application.h"
#include "Event.h"
#include "Window.h"
#include "Renderer.h"
#include "EntityMediator.h"

Application::Application() : m_Window(new Window())
{
	m_Window->SetEventCallback(std::bind(&Application::OnEvent, this, std::placeholders::_1));
	m_Running = true;
}

Application::Application(const WindowData& windowData) : m_Window(new Window(windowData))
{
	m_Window->SetEventCallback(std::bind(&Application::OnEvent, this, std::placeholders::_1));
	m_Running = true;
}

Application::~Application()
{

}

bool Application::OnEvent(Event& e)
{
	EventDispatcher dispatcher(e);
	m_EntityMediator->PassEventToEntities(e);
	return true;
}

void Application::Initialize()
{
	m_EntityMediator->InitializeEntities();
}

void Application::Update()
{
	Renderer::GetSingleton()->Clear();
	MakeContextCurrent();
	m_EntityMediator->UpdateEntities();
	GetWindow()->Update();
}

void Application::Run()
{
	while (m_Running)
	{
		Update();
	}
}

void Application::Close()
{
	m_Running = false;
	m_Window->Finalize();
}

void Application::MakeContextCurrent() 
{
	m_Window->MakeContextCurrent();
}

void Application::FlushCurrentContext() 
{
	m_Window->FlushCurrentContext();
}
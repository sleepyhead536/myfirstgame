#include <iostream>
#include <functional>
#include "MainApplication.h"
#include "ApplicationEvent.h"
#include "EntityMediator.h"
#include "Window.h"

MainApplication* MainApplication::m_Instance = nullptr;

MainApplication* MainApplication::GetSingleton()
{
	if (!m_Instance)
	{
		m_Instance = new MainApplication;
	}
	return m_Instance;
}
MainApplication::MainApplication()
{
}
MainApplication::~MainApplication()
{
}
bool MainApplication::OnEvent(Event& e)
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<WindowCloseEvent>(std::bind(&MainApplication::OnWindowClose, this, std::placeholders::_1));
	m_EntityMediator->PassEventToEntities(e);
	return true;
}

bool MainApplication::OnWindowClose(Event& e)
{
	std::cout << e.ToString() << std::endl;
	Close();
	return true;
}

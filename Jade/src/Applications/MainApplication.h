#pragma once
#include "Application.h"

class MainApplication : public Application
{
public:
	MainApplication();
	~MainApplication();
	bool OnEvent(Event& e);
	static MainApplication* GetSingleton();

private:
	bool OnWindowClose(Event& e);
	static MainApplication* m_Instance;
};

#pragma once

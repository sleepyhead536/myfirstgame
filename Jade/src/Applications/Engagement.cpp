#include <iostream>
#include <functional>
#include <string>
#include "Engagement.h"
#include "EntityMediator.h"
#include "Enemy.h"
#include "Event.h"
#include "KeyEvent.h"
#include "KeyboardMappings.h"
#include "AttackBar.h"
#include "Background.h"
#include "Window.h"

Engagement::Engagement(const WindowData& windowData, Enemy* enemy) : 
	Application(windowData), m_EntityFactory(new EntityFactory), m_Enemy(enemy)
{
}

Engagement::~Engagement()
{
}

bool Engagement::OnEvent(Event& e)
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<KeyPressedEvent>(std::bind(&Engagement::OnKeyPressed, this, std::placeholders::_1));
	m_EntityMediator->PassEventToEntities(e);
	return true;
}

void Engagement::Initialize()
{

    m_EntityFactory->SetMediator(*m_EntityMediator);

	Background::Input backgroundInput{ 0.0f, 0.0f, 2.0f, 2.0f, "res/images/engagementBackground.png"};
	m_EntityFactory->CreateEntity<Background>((void*)& backgroundInput);

	Background::Input hitboxInput{ 0.0f, 0.0f, 0.75f, 0.15f, "res/images/hitbox.png"};
	m_EntityFactory->CreateEntity<Background>((void*)& hitboxInput);

	AttackBar::Input attackBarInput{ -0.25f, 0.25f, 0.0f, 0.25f, 0.50f};
	m_EntityFactory->CreateEntity<AttackBar>((void*)& attackBarInput);

	for (int i=0; i<m_Enemy->GetHealth(); ++i)
	{
		float verticalPosition = -0.5 + i*0.25;
		Background::Input heart{ verticalPosition, 0.25f, 0.25f, 0.25f, "res/images/heart.png"};
		Entity* entity = m_EntityFactory->CreateEntity<Background>((void*)& heart);
		m_HeartIDs.push_back(entity->GetID());
	}

	m_EntityMediator->InitializeEntities();
}

void Engagement::Update()
{
    Application::Update();
}

bool Engagement::OnKeyPressed(Event& e)
{
	KeyPressedEvent event = *(KeyPressedEvent*)& e;
	int keycode = event.GetKeyCode();
	if (keycode == KeyboardMappings::SPACEBAR)
	{
	    unsigned int id = m_HeartIDs.back();
	    m_EntityFactory->DestroyEntity(id);
	    m_HeartIDs.pop_back();
		if (m_HeartIDs.size() == 0)
		{
			Close();
		}
	}

	m_Enemy->OnEvent(e);
}
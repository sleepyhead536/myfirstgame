#pragma once

class Event;
class Window;
class WindowData;
class EntityMediator;

class Application
{
public:
	Application();
	Application(const WindowData& windowData);
	~Application();
	virtual bool OnEvent(Event& e);
	virtual void Initialize();
	virtual void Update();
	void Run();
	void Close();
	void MakeContextCurrent();
	void FlushCurrentContext();
	void SetEntityMediator(EntityMediator* mediator) { m_EntityMediator = mediator; }
	Window* GetWindow() { return m_Window; }

protected:
	Window* m_Window;
	EntityMediator* m_EntityMediator;
	bool m_Running;
};


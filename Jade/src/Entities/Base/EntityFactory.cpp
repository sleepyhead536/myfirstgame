#include <stdlib.h>
#include "EntityFactory.h"

std::vector<Entity*> EntityFactory::GetEntitiesOfType(EntityType type)
{
	std::vector<Entity*> entities;
	for (std::vector<Entity*>::iterator iter = m_Entities.begin(); iter != m_Entities.end(); ++iter)
	{
		if ((*iter)->GetEntityType() == type)
		{
			entities.push_back(*iter);
		}
	}
	return entities;
}

Entity* EntityFactory::GetEntityByID(unsigned int id)
{
	for (std::vector<Entity*>::iterator iter = m_Entities.begin(); iter != m_Entities.end(); ++iter)
	{
		if ((*iter)->GetID() == id)
		{
			return (*iter);
		}			
	}
	return NULL;
}

void EntityFactory::Free()
{
	for (std::vector<Entity*>::iterator iter = m_Entities.begin(); iter != m_Entities.end(); ++iter)
	{
		free(*iter);
	}
}

void EntityFactory::DestroyEntity(unsigned int id)
{
	DestroyEntity(GetEntityByID(id));
}


void EntityFactory::DestroyEntity(Entity* entity)
{
	for (std::vector<Entity*>::iterator iter = m_Entities.begin(); iter != m_Entities.end(); ++iter)
    {
    	if ((*iter)->GetID() == entity->GetID())
    	{
    	 	m_Entities.erase(iter);
			break;
    	}			
    }

	m_Mediator->DestroyClient(m_EntityToClientMap[entity->GetID()]);
	m_EntityToClientMap.erase(entity->GetID());
	free(entity);
}


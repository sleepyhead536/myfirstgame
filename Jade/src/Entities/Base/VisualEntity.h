#pragma once
#include "EntityFactory.h"

class Texture;
class Shader;
class Renderer;

class VisualEntity : public Entity
{
public:
	VisualEntity();
	~VisualEntity();
	virtual EntityType GetEntityType() const { return EntityType::None; };
	virtual bool OnEvent(Event& e);

	void SetPosition(const float& x, const float& y);
	void SetWidth(const float& width) { m_Width = width; }
	void SetHeight(const float& height) { m_Height = height; }
	void SetTexture(Texture* texture) { m_Texture = texture; }
	void SetShader(Shader* shader) { m_Shader = shader; }
	void SetRenderer(Renderer* renderer) { m_Renderer = renderer; }
	float GetX() { return m_Position[0]; }
	float GetY() { return m_Position[1]; }
	float GetWidth() { return m_Width; }
	float GetHeight() { return m_Height; }
	void Draw();

protected:
	float m_Position[2], m_Width, m_Height;
	Texture* m_Texture;
	Shader* m_Shader;
	Renderer* m_Renderer;

	void FillVertexBuffer(void* buffer);

};
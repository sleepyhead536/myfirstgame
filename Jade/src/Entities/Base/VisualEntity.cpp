#include <string.h>
#include <iostream>
#include "VisualEntity.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "Renderer.h"
#include "Shader.h"
#include "Texture.h"

VisualEntity::VisualEntity()
	: m_Position{ 0.0f, 0.0f }, m_Width(0.0f), m_Height(0.0f),
	m_Texture(nullptr), m_Shader(nullptr), m_Renderer(nullptr)
{
}

VisualEntity::~VisualEntity()
{
}

bool VisualEntity::OnEvent(Event& e)
{
	return true;
}

void VisualEntity::SetPosition(const float& x, const float& y)
{
	float newPosition[2] = { x,y };
	memcpy((void*)& m_Position, (void*)& newPosition, sizeof(float) * 2);
}

void VisualEntity::Draw()
{
	if (m_Texture == nullptr)
	{
		std::cout << "Drawing visual entity without texture." << std::endl;
		ASSERT(false);
	}
	float positions[16];
	FillVertexBuffer(positions);

	GLCall(glEnable(GL_BLEND));
	GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

	VertexBuffer vb(positions, 4 * 4 * sizeof(float));
	VertexBufferLayout layout;
	layout.PushFloats(2);
	layout.PushFloats(2);
	VertexArray va;
	va.AddBuffer(vb, layout);

	unsigned char indices[6] = {
		0, 1, 2,
		2, 3, 0
	};
	IndexBuffer ib(indices, 6);

	m_Shader->Bind();
	m_Shader->SetUniform1i("u_Texture", 0);
	m_Texture->Bind();
	m_Renderer->Draw(va, ib);
	m_Shader->Unbind();
	m_Texture->Unbind();
}

void VisualEntity::FillVertexBuffer(void* buffer)
{
	// Supplies verticies from the bottom left counter clockwise
	float positions[16] = {
		m_Position[0] - (m_Width / 2.0f), m_Position[1] - (m_Height / 2.0f), 0.0f, 0.0f,
		m_Position[0] + (m_Width / 2.0f), m_Position[1] - (m_Height / 2.0f), 1.0f, 0.0f,
		m_Position[0] + (m_Width / 2.0f), m_Position[1] + (m_Height / 2.0f), 1.0f, 1.0f,
		m_Position[0] - (m_Width / 2.0f), m_Position[1] + (m_Height / 2.0f), 0.0f, 1.0f,
	};

	memcpy(buffer, positions, sizeof(float) * 4 * 4);
}
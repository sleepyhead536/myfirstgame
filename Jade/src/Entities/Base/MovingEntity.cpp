#include <string.h>
#include "MovingEntity.h"
#include "Engine/PhysicsEngine.h"

MovingEntity::MovingEntity()
	: VisualEntity()
{
}

MovingEntity::~MovingEntity()
{
}

bool MovingEntity::OnEvent(Event& e)
{
	return true;
}

void MovingEntity::Move(const float& angleDegClockWiseFromNorth, const float& magnitude)
{
	float x = m_Position[0];
	float y = m_Position[1];
	PhysicsEngine::GetSingleton()->MoveObject(x, y, magnitude, angleDegClockWiseFromNorth);

	memcpy((void*)&m_Position[0], (void*)&x, sizeof(float));
	memcpy((void*)&m_Position[1], (void*)&y, sizeof(float));
}
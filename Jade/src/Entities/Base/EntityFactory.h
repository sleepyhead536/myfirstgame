#pragma once
#include <vector>
#include <map>
#include <functional>
#include "EntityMediator.h"

class Event;
class EntityFactory;

static unsigned int EntityCounter = 0;

enum class EntityType {
	None = 0,
	Background,
	Character,
	Collectible,
	Obstacle,
	Physics
};

class Entity
{
public:
	Entity() : m_ID(++EntityCounter) {};
	~Entity() {};
	const unsigned int GetID() const { return m_ID; }
	virtual EntityType GetEntityType() const = 0;
	virtual bool OnEvent(Event& e) = 0;
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	void SetFactory(EntityFactory& factory) { m_Factory = &factory; }
	void SetMediator(EntityMediator& mediator) { m_Mediator = &mediator; }

protected:
	unsigned int m_ID;
	EntityFactory* m_Factory;
	EntityMediator* m_Mediator;
};

class EntityFactory
{
public:
	EntityFactory() {};
	~EntityFactory() {};
	std::vector<Entity*> GetEntities() { return m_Entities; }
	std::vector<Entity*> GetEntitiesOfType(EntityType type);
	Entity* GetEntityByID(unsigned int id);
	void Free();
	void SetMediator(EntityMediator& mediator) { m_Mediator = &mediator;}
	void DestroyEntity(unsigned int id);
	void DestroyEntity(Entity* entity);

	template<typename Type>
	Type* CreateEntity(void* input)
	{
		Type* entity = new Type(input);
		entity->SetFactory(*this);
		entity->SetMediator(*m_Mediator);
		m_Entities.push_back(entity);
		EntityClient* client = new EntityClient();
		client->SetInitializeCallback(std::bind(&Type::Initialize, entity));
		client->SetUpdateCallback(std::bind(&Type::Update, entity));
		client->SetOnEventCallback(std::bind(&Type::OnEvent, entity, std::placeholders::_1));
		m_Mediator->RegisterClient(*client);
		m_EntityToClientMap.insert(std::pair<unsigned int, unsigned int>(entity->GetID(), client->GetID()));
		return entity;
	}

private:

	std::vector<Entity*> m_Entities;
	std::map<unsigned int, unsigned int> m_EntityToClientMap;
	EntityMediator* m_Mediator;
};


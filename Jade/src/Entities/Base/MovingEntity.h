#pragma once
#include "VisualEntity.h"

class MovingEntity : public VisualEntity
{
public:
	MovingEntity();
	~MovingEntity();
	virtual EntityType GetEntityType() const { return EntityType::None; };
	virtual bool OnEvent(Event& e);

	void Move(const float& angleDegClockWiseFromNorth, const float& magnitude);

private:

};
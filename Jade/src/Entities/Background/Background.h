#pragma once
#include <string>
#include "VisualEntity.h"

class Event;
class Sprite;

class Background : public VisualEntity
{
public:
	struct Input {
		const float x;
		const float y;
		const float width;
		const float height;
		std::string image;
	};

	Background(void* Input);
	~Background();
	EntityType GetEntityType() const { return EntityType::Background; };
	bool OnEvent(Event& e);
	void Initialize();
	void Update();

	void SetImage(std::string& image) { m_Image = image; }

private:
	Sprite* m_Sprite;
	std::string m_Image;
};
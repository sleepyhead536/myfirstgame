#include "Background.h"
#include "Sprite.h"
#include "Shader.h"
#include "Renderer.h"
#include "Event.h"

Background::Background(void* in) :
	VisualEntity()
{
	Input input = *(Input*)in;
	SetPosition(input.x, input.y);
	SetWidth(input.width);
	SetHeight(input.height);
	SetImage(input.image);
}

Background::~Background()
{
}

void Background::Initialize()
{
	m_Sprite = new Sprite(m_Image, 1);
	m_Sprite->Initialize();

	SetTexture(m_Sprite->GetTexture());
	SetShader(new Shader("res/shaders/Texture.shader"));
	SetRenderer(Renderer::GetSingleton());

	m_Shader->Initialize();
}

bool Background::OnEvent(Event& e)
{
	return true;
}

void Background::Update()
{
	Draw();
}

set (target Background)

set (sources 
    Background.cpp
)

set (headers
    Background.h
)

add_library(${target} ${sources} ${headers})

target_include_directories(Background
    PRIVATE ${CMAKE_SOURCE_DIR}/src/Entities/Base
    PRIVATE ${CMAKE_SOURCE_DIR}/src/Events
    PRIVATE ${CMAKE_SOURCE_DIR}/src/Infrastructure
    PRIVATE ${CMAKE_SOURCE_DIR}/src/Mediators
    PRIVATE ${CMAKE_SOURCE_DIR}/src/Renderer
    PRIVATE ${CMAKE_SOURCE_DIR}/include
)

target_link_libraries(Background
    PRIVATE EntityBase
    PRIVATE Mediators
    PRIVATE Renderer
    PRIVATE ${GRAPHICS_LIBRARIES}
)

#pragma once
#include <map>
#include "Obstacle.h"

class Engagement;
class Mediator;

class Enemy : public Obstacle
{
public:
	struct Input : public Obstacle::Input {
		const unsigned int health;
        Input(const float x, const float y, const float width, 
            const float height, const std::string image, const unsigned int health) : 
            Obstacle::Input({x, y, width, height, image}), health(health) 
        {
        }

	};

    enum STATUS 
    {
        IDLE,
        ENGAGED,
        DEAD   
    };

    Enemy(void* input);
    ~Enemy();

    unsigned int GetHealth() const { return m_Health; }

	EntityType GetEntityType() const { return EntityType::Obstacle; };
    void Initialize();
    void Update();
    bool OnEvent(Event& e);

    bool OnCollision(Event& e);
    bool OnKeyPressed(Event& e);
    bool OnEngagementEvent(Event& e);
    void SpawnEngagement();

private:
    STATUS m_Status;
    bool m_EngagementSpawned;
    unsigned int m_Health;
    EntityMediator* m_EntityMediator;
    Engagement* m_Engagement;
};
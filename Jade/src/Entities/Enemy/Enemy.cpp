#include <string.h>
#include <iostream>
#include "Enemy.h"
#include "Sprite.h"
#include "Shader.h"
#include "Renderer.h"
#include "CollisionEvent.h"
#include "KeyEvent.h"
#include "KeyboardMappings.h"
#include "Character.h"
#include "Window.h"
#include "MainApplication.h"
#include "Engagement.h"
#include "PuzzleSolvedEvent.h"

Enemy::Enemy(void* in) : Obstacle(in), m_Status(STATUS::IDLE), m_EntityMediator(new EntityMediator())
{
	Input input = *(Input*)in;
	SetPosition(input.x, input.y);
	SetWidth(input.width);
	SetHeight(input.height);
	m_Health = input.health;
	m_Sprites[ObstacleSpriteSequence::Up] =    new Sprite(input.image, 1);
	m_Sprites[ObstacleSpriteSequence::Left] =  new Sprite(input.image, 1);
	m_Sprites[ObstacleSpriteSequence::Down] =  new Sprite(input.image, 1);
	m_Sprites[ObstacleSpriteSequence::Right] = new Sprite(input.image, 1);
}

Enemy::~Enemy()
{
	delete(m_EntityMediator);
}

void Enemy::Initialize()
{
    Obstacle::Initialize();
}
bool Enemy::OnEvent(Event& e) 
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<CollisionEvent>(std::bind(&Enemy::OnCollision, this, std::placeholders::_1));
	dispatcher.Dispatch<KeyPressedEvent>(std::bind(&Enemy::OnKeyPressed, this, std::placeholders::_1));
	return true;
}

void Enemy::Update()
{
	switch(m_Status)
	{
		case (STATUS::IDLE):
		{
    		Obstacle::Update();
			break;
		}
		case (STATUS::ENGAGED):
		{
		    m_Engagement->MakeContextCurrent();
		    m_Engagement->Update();
		    MainApplication::GetSingleton()->MakeContextCurrent();
    		Obstacle::Update();
			break;
		}
		case (STATUS::DEAD):
		{
			m_Factory->DestroyEntity(this);
			break;
		}
	}
}

bool Enemy::OnCollision(Event& e)
{
	CollisionEvent event = *(CollisionEvent*)& e;
	if (event.GetEntityOneID() != m_ID && event.GetEntityTwoID() != m_ID)
	{
		return true;
	}
	unsigned int intruderID;
	if (event.GetEntityOneID() == m_ID)
	{
		intruderID = event.GetEntityTwoID();
	}
	else 
	{
		intruderID = event.GetEntityOneID();
	}	
	std::cout << "Enemy: " << m_ID << " collided with " << intruderID << "\n";
	Entity* intruder = m_Factory->GetEntityByID(intruderID);
    if (intruder->GetEntityType() == EntityType::Character)
    {
        SpawnEngagement();
    }
	return true;
}

void Enemy::SpawnEngagement()
{
	if (m_Status == STATUS::IDLE)
	{
        WindowData windowData {"Enemy Engagement", 340, 340, std::bind(&Enemy::OnEvent, this, std::placeholders::_1), MainApplication::GetSingleton()->GetWindow()};
        m_Engagement = new Engagement(windowData, this);
        m_Engagement->SetEntityMediator(m_EntityMediator);
        m_Engagement->Initialize();

        MainApplication::GetSingleton()->MakeContextCurrent();
        m_Status = STATUS::ENGAGED;
	}
}

bool Enemy::OnEngagementEvent(Event& e) 
{
    std::cout << "Enemy received event: " << e.ToString() << "\n";
	return true;
}

bool Enemy::OnKeyPressed(Event& e)
{
	if (m_Status == STATUS::ENGAGED)
	{
	    KeyPressedEvent event = *(KeyPressedEvent*)& e;
	    int keycode = event.GetKeyCode();
	    if (keycode == KeyboardMappings::SPACEBAR)
	    {
			m_Health -= 1;
			if (m_Health == 0)
			{
				m_Status = STATUS::DEAD;
			}
	    }
	}
}
#pragma once
#include <map>
#include "MovingEntity.h"

class Event;
class Sprite;

enum class AttackBarNeedleDirection {
    Default = 0,
    Left, Right
};

class AttackBar : public MovingEntity
{
public:
	struct Input {
		const float x_min;
		const float x_max;
		const float y;
		const float width;
		const float height;
	};

	AttackBar(void* Input);
	~AttackBar();
	EntityType GetEntityType() const { return EntityType::Background; };
	bool OnEvent(Event& e);
	void Initialize();
	void Update();
    void SetRange(const float& min, const float& max) { m_Xmin = min; m_Xmax = max; };
	bool OnKeyPressed(Event& e);

private:
    float m_Xmin, m_Xmax;
    Sprite* m_Needle;
    int m_Slots, m_SlotCounter;
    AttackBarNeedleDirection m_Direction;
    void UpdateNeedlePosition();
};
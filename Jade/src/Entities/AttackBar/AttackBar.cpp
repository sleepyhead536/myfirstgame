#include <string.h>
#include <iostream>
#include <cmath>
#include "AttackBar.h"
#include "KeyboardMappings.h"
#include "KeyEvent.h"
#include "Sprite.h"
#include "Shader.h"
#include "Renderer.h"

#define PI 3.14159265358979323846 

AttackBar::AttackBar(void* in) :
	MovingEntity(), m_Needle(nullptr), m_Slots(20), m_SlotCounter(0), m_Direction(AttackBarNeedleDirection::Right)
{
	Input input = *(Input*)in;
	SetRange(input.x_min, input.x_max);
    SetPosition(input.x_min, input.y);
	SetWidth(input.width);
	SetHeight(input.height);
}

AttackBar::~AttackBar()
{
}

void AttackBar::Initialize()
{
	m_Needle = new Sprite("res/images/needle.png", 1);

    m_Needle->Initialize();

	SetTexture(m_Needle->GetTexture());
	SetShader(new Shader("res/shaders/Texture.shader"));
	SetRenderer(Renderer::GetSingleton());

	m_Shader->Initialize();
}

bool AttackBar::OnEvent(Event& e) 
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<KeyPressedEvent>(std::bind(&AttackBar::OnKeyPressed, this, std::placeholders::_1));
	return true;
}

void AttackBar::Update()
{
    UpdateNeedlePosition();
	SetTexture(m_Needle->GetTexture());
	Draw();
}

void AttackBar::UpdateNeedlePosition()
{
    if (m_Direction == AttackBarNeedleDirection::Right)
    {
        ++m_SlotCounter;
        if (m_SlotCounter == m_Slots)
        {
            m_Direction = AttackBarNeedleDirection::Left;
        }
    }
    else
    {
        --m_SlotCounter;
        if (m_SlotCounter == 0)
        {
            m_Direction = AttackBarNeedleDirection::Right;
        }
    }

    float t = PI * ((float) m_SlotCounter / (float) m_Slots);
    float x = (-cos(t) + 0.5) * (m_Xmax - m_Xmin) + m_Xmin;
    float y = m_Position[1];
    SetPosition(x, y);
}

bool AttackBar::OnKeyPressed(Event& e)
{
	KeyPressedEvent event = *(KeyPressedEvent*)& e;
	int keycode = event.GetKeyCode();
	std::cout << "Entity: " << m_ID << " recieved key pressed event" << std::endl;
	std::cout << "Entity: " << m_ID << " is at " << m_Position[0] << ", " << m_Position[1] << std::endl;

	if (keycode == KeyboardMappings::SPACEBAR)
    {
        m_SlotCounter = 0;
        m_Direction = AttackBarNeedleDirection::Right;
    }
	return true;
}


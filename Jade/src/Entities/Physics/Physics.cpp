#include "Physics.h"
#include "Engine/PhysicsEngine.h"
#include "VisualEntity.h"
#include "Character.h"
#include "Obstacle.h"
#include "Mediator.h"
#include "CollisionEvent.h"

Physics::Physics(void* in)
{
}
Physics::~Physics()
{

}
bool Physics::OnEvent(Event& e)
{
    return true;
}
void Physics::Initialize()
{

}
void Physics::Update()
{
    CheckForCollisions();
}
void Physics::CheckForCollisions()
{
	std::vector<Entity*> characters = m_Factory->GetEntitiesOfType(EntityType::Character);
    Character character = *(Character*)(characters[0]);

	std::vector<Entity*> obstacles = m_Factory->GetEntitiesOfType(EntityType::Obstacle);
	for (std::vector<Entity*>::iterator iter = obstacles.begin(); iter != obstacles.end(); ++iter)
	{
        Obstacle obstacle = *(Obstacle*)(*iter);
		if(CheckForEntityCollision(obstacle, character))
		{
			std::cout << "Character is colliding with obstacle: " << obstacle.GetID() << "\n";
            CollisionEvent event = CollisionEvent(character.GetID(), obstacle.GetID());
			m_Mediator->PassEventToEntities(event);
		}
	}
}
bool Physics::CheckForEntityCollision(VisualEntity& A, VisualEntity& B)
{
    float verticalEdges[4];
    verticalEdges[0] = A.GetX() - A.GetWidth() / 2;
    verticalEdges[1] = A.GetX() + A.GetWidth() / 2;
    verticalEdges[2] = B.GetX() - B.GetWidth() / 2;
    verticalEdges[3] = B.GetX() + B.GetWidth() / 2;

    float horizontalEdges[4];
    horizontalEdges[0] = A.GetY() - A.GetHeight() / 2;
    horizontalEdges[1] = A.GetY() + A.GetHeight() / 2;
    horizontalEdges[2] = B.GetY() - B.GetHeight() / 2;
    horizontalEdges[3] = B.GetY() + B.GetHeight() / 2;
    return PhysicsEngine::GetSingleton()->CheckFor2DCollision(verticalEdges, horizontalEdges);
}



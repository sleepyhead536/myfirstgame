#pragma once

class PhysicsEngine
{
public:
    ~PhysicsEngine();
	static PhysicsEngine* GetSingleton();
    bool CheckFor2DCollision(float* verticalEdges, float* horizontalEdges);
    bool CheckFor1DCollision(float* A, float* B);
    void MoveObject(float& x, float& y, const float& magnitude, const float& angleDegClockWiseFromNorth);
private:
    PhysicsEngine() {};
	PhysicsEngine(PhysicsEngine const&) {};
	PhysicsEngine& operator=(PhysicsEngine const&) {};
	static PhysicsEngine* m_Instance;
};
#include "PhysicsEngine.h"

#include <math.h>
#define PI 3.14159265f

PhysicsEngine* PhysicsEngine::m_Instance = nullptr;

PhysicsEngine* PhysicsEngine::GetSingleton()
{
	if (!m_Instance)
	{
		m_Instance = new PhysicsEngine;
	}
	return m_Instance;
}
bool PhysicsEngine::CheckFor2DCollision(float* verticalEdges, float* horizontalEdges)
{
    bool verticalCheck = CheckFor1DCollision(verticalEdges, &verticalEdges[2]);
    bool horizontalCheck = CheckFor1DCollision(horizontalEdges, &horizontalEdges[2]);

    if (verticalCheck && horizontalCheck)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}
bool PhysicsEngine::CheckFor1DCollision(float* firstSet, float* secondSet)
{
    if (((firstSet[0] < secondSet[0]) && (firstSet[1] < secondSet[0])) ||
        ((firstSet[0] > secondSet[1]) && (firstSet[1] > secondSet[1]))) 
    {
        return false;
    }
    else
    {
        return true;
    }
    
}

void PhysicsEngine::MoveObject(float& x, float& y, const float& magnitude, const float& angleDegClockWiseFromNorth)
{
	float angleClockWiseFromNorth = angleDegClockWiseFromNorth * PI / 180;
	x += sin(angleClockWiseFromNorth) * magnitude;
	y += cos(angleClockWiseFromNorth) * magnitude;
}
#pragma once
#include <string>
#include "EntityFactory.h"

class VisualEntity;

class Physics : public Entity
{
public:
    Physics(void* input);
    ~Physics();
    EntityType GetEntityType() const { return EntityType::Physics; };
    bool OnEvent(Event& e);
    void Initialize();
    void Update();
private:

    void CheckForCollisions();
    bool CheckForEntityCollision(VisualEntity& A, VisualEntity& B);
};
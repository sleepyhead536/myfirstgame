#pragma once
#include <map>
#include "MovingEntity.h"

class Event;
class Sprite;

enum class ObstacleSpriteSequence {
	Default = 0,
	Up, Left, Down, Right
};

class Obstacle : public MovingEntity
{
public:
	struct Input {
		const float x;
		const float y;
		const float width;
		const float height;
		const std::string image;
	};

	Obstacle(void* Input);
	~Obstacle();
	EntityType GetEntityType() const { return EntityType::Obstacle; };
	virtual bool OnEvent(Event& e);
	virtual void Initialize();
	virtual void Update();

	virtual bool OnCollision(Event& e);

    void SetImage(int index, std::string& image);
	typedef std::map<ObstacleSpriteSequence, Sprite*> SpriteMap;

protected:
	SpriteMap m_Sprites;
	ObstacleSpriteSequence m_SpriteIdx;
	unsigned int m_MovementsPerSprite, m_MovementCount;
};

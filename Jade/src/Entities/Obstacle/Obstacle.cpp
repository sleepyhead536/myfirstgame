#include <string.h>
#include <iostream>
#include "Obstacle.h"
#include "Sprite.h"
#include "Shader.h"
#include "Renderer.h"
#include "CollisionEvent.h"
#include "Character.h"

Obstacle::Obstacle(void* in) :
	MovingEntity(), m_SpriteIdx(ObstacleSpriteSequence::Left), m_MovementsPerSprite(2), m_MovementCount(0)
{
	Input input = *(Input*)in;
	SetPosition(input.x, input.y);
	SetWidth(input.width);
	SetHeight(input.height);
	m_Sprites[ObstacleSpriteSequence::Up] =    new Sprite(input.image, 1);
	m_Sprites[ObstacleSpriteSequence::Left] =  new Sprite(input.image, 1);
	m_Sprites[ObstacleSpriteSequence::Down] =  new Sprite(input.image, 1);
	m_Sprites[ObstacleSpriteSequence::Right] = new Sprite(input.image, 1);
}

Obstacle::~Obstacle()
{
}

void Obstacle::Initialize()
{
	for (SpriteMap::iterator iter = m_Sprites.begin(); iter != m_Sprites.end(); ++iter)
	{
		iter->second->Initialize();
	}

	SetTexture(m_Sprites[m_SpriteIdx]->GetTexture());
	SetShader(new Shader("res/shaders/Texture.shader"));
	SetRenderer(Renderer::GetSingleton());

	m_Shader->Initialize();
}

bool Obstacle::OnEvent(Event& e) 
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<CollisionEvent>(std::bind(&Obstacle::OnCollision, this, std::placeholders::_1));
	return true;
}

void Obstacle::Update()
{
	Draw();
}

bool Obstacle::OnCollision(Event& e)
{
	CollisionEvent event = *(CollisionEvent*)& e;
	if (event.GetEntityOneID() != m_ID && event.GetEntityTwoID() != m_ID)
	{
		return true;
	}
	unsigned int intruderID;
	if (event.GetEntityOneID() == m_ID)
	{
		intruderID = event.GetEntityTwoID();
	}
	else 
	{
		intruderID = event.GetEntityOneID();
	}	
	std::cout << "Obstacle: " << m_ID << " collided with " << intruderID << "\n";
	Entity* intruder = m_Factory->GetEntityByID(intruderID);
	Character character = *(dynamic_cast<Character*>(intruder));
	m_SpriteIdx = static_cast<ObstacleSpriteSequence>(character.GetMovementDirection());
	switch(m_SpriteIdx)
	{
		case ObstacleSpriteSequence::Up:
		{
		    Move(0, 0.03f);
			break;
		}
		case ObstacleSpriteSequence::Left:
		{
		    Move(270, 0.03f);
			break;
		}
		case ObstacleSpriteSequence::Down:
		{
     		Move(180, 0.03f);
			break;
		}
		case ObstacleSpriteSequence::Right:
		{
		    Move(90, 0.03f);
		    break;
		}
	}

	++m_MovementCount;
	if (m_MovementCount == m_MovementsPerSprite)
	{
		m_Sprites[m_SpriteIdx]->NextImage();
		m_MovementCount = 0;
	}

	SetTexture(m_Sprites[m_SpriteIdx]->GetTexture());
	return true;
}

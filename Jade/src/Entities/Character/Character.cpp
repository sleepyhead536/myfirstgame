#include <string.h>
#include <iostream>
#include "Character.h"
#include "KeyboardMappings.h"
#include "KeyEvent.h"
#include "Sprite.h"
#include "Shader.h"
#include "Renderer.h"

Character::Character(void* in) :
	MovingEntity(), m_SpriteIdx(CharacterSpriteSequence::Left), m_MovementsPerSprite(2), m_MovementCount(0)
{
	Input input = *(Input*)in;
	SetPosition(input.x, input.y);
	SetWidth(input.width);
	SetHeight(input.height);
}

Character::~Character()
{
}

void Character::Initialize()
{
	m_Sprites[CharacterSpriteSequence::Up] = new Sprite("res/images/upwardSequence.png", 4);
	m_Sprites[CharacterSpriteSequence::Left] = new Sprite("res/images/leftSequence.png", 4);
	m_Sprites[CharacterSpriteSequence::Down] = new Sprite("res/images/downwardSequence.png", 4);
	m_Sprites[CharacterSpriteSequence::Right] = new Sprite("res/images/rightSequence.png", 4);

	for (SpriteMap::iterator iter = m_Sprites.begin(); iter != m_Sprites.end(); ++iter)
	{
		iter->second->Initialize();
	}

	SetTexture(m_Sprites[m_SpriteIdx]->GetTexture());
	SetShader(new Shader("res/shaders/Texture.shader"));
	SetRenderer(Renderer::GetSingleton());

	m_Shader->Initialize();
}

bool Character::OnEvent(Event& e) 
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<KeyPressedEvent>(std::bind(&Character::OnKeyPressed, this, std::placeholders::_1));
	return true;
}

void Character::Update()
{
	Draw();
}

bool Character::OnKeyPressed(Event& e)
{
	KeyPressedEvent event = *(KeyPressedEvent*)& e;
	int keycode = event.GetKeyCode();
	std::cout << "Entity: " << m_ID << " recieved key pressed event" << std::endl;
	std::cout << "Entity: " << m_ID << " is at " << m_Position[0] << ", " << m_Position[1] << std::endl;

	switch (keycode)
	{
	case KeyboardMappings::W:
		m_SpriteIdx = CharacterSpriteSequence::Up;
		Move(0, 0.03f);
		break;
	case KeyboardMappings::A:
		m_SpriteIdx = CharacterSpriteSequence::Left;
		Move(270, 0.03f);
		break;
	case KeyboardMappings::S:
		m_SpriteIdx = CharacterSpriteSequence::Down;
		Move(180, 0.03f);
		break;
	case KeyboardMappings::D:
		m_SpriteIdx = CharacterSpriteSequence::Right;
		Move(90, 0.03f);
		break;
	}

	++m_MovementCount;
	if (m_MovementCount == m_MovementsPerSprite)
	{
		m_Sprites[m_SpriteIdx]->NextImage();
		m_MovementCount = 0;
	}

	SetTexture(m_Sprites[m_SpriteIdx]->GetTexture());
	return true;
}

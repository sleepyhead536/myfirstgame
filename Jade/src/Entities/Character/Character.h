#pragma once
#include <map>
#include "MovingEntity.h"

class Event;
class Sprite;

enum class CharacterSpriteSequence {
	Default = 0,
	Up, Left, Down, Right
};

class Character : public MovingEntity
{
public:
	struct Input {
		const float x;
		const float y;
		const float width;
		const float height;
	};

	Character(void* Input);
	~Character();
	EntityType GetEntityType() const { return EntityType::Character; };
	bool OnEvent(Event& e);
	void Initialize();
	void Update();

	bool OnKeyPressed(Event& e);
	CharacterSpriteSequence GetMovementDirection() { return m_SpriteIdx; }

	typedef std::map<CharacterSpriteSequence, Sprite*> SpriteMap;

private:
	SpriteMap m_Sprites;
	CharacterSpriteSequence m_SpriteIdx;
	unsigned int m_MovementsPerSprite, m_MovementCount;
};
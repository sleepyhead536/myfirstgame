#pragma once
#include <string>
#include <vector>

class Texture;
class Shader;

class Sprite
{
private:
	std::string m_Filename;
	int m_Width, m_Height, m_SpriteIndex, m_NumOfImages;
	unsigned int m_Slot;
	unsigned char* m_Buffer;
	std::vector<Texture*> m_Textures;

public:
	Sprite(const std::string& filename, const int images);
	Sprite();
	~Sprite();

	Texture* GetTexture() { return m_Textures[m_SpriteIndex]; };
	int GetWidth() { return m_Width; }
	int GetHeight() { return m_Height; }

	void Initialize();
	void NextImage();
	void Bind(Shader& shader);
	void Unbind();
};
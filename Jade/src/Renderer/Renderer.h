#pragma once
#include "Core.h"
#include "Logging.h"

#define GLCall(x) GLClearError();\
	x; ASSERT(GLLogCall(#x, __FILE__, __LINE__))

void GLClearError();
bool GLLogCall(const char* function, const char* file, int line);

class VertexArray;
class IndexBuffer;
class Shader;

class Renderer
{
public:
	~Renderer() {};
	static Renderer* GetSingleton();
	void Draw(const VertexArray& va, const IndexBuffer& ib) const;
	void Clear() const;

private:
	Renderer() {};
	Renderer(Renderer const&) {};
	Renderer& operator=(Renderer const&) {};

	static Renderer* m_Instance;
};
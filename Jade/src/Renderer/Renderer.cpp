#include <iostream>
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Renderer.h"


void GLClearError()
{
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		std::cout << "[Open GL Error]: " << error << " "
			<< file << ":" << line << " " << function << std::endl;
		return false;
	}
	return true;
}

Renderer* Renderer::m_Instance = nullptr;

Renderer* Renderer::GetSingleton()
{
	if (!m_Instance)
	{
		m_Instance = new Renderer;
	}
	return m_Instance;
}

void Renderer::Draw(const VertexArray& va, const IndexBuffer& ib) const
{
	va.Bind();
	ib.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_BYTE, nullptr));
}

void Renderer::Clear() const
{
	glClear(GL_COLOR_BUFFER_BIT);
}

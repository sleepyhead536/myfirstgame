#pragma once

#include <string>

class Texture
{
private:
	unsigned int m_RendererID;
	unsigned char* m_Buffer;

public:
	Texture();
	~Texture();

	void Initialize(unsigned char* buffer, const int width, const int height, const int bitsperpixel);
	void Bind(unsigned int slot=0) const;
	void Unbind() const;
	void Free() const;
};


#include <string>
#include <string.h>
#include <iostream>
#include "Sprite.h"
#include "Shader.h"
#include "Texture.h"
#include "stb_image.h"

Sprite::Sprite() :
	m_Filename(""), m_Width(0), m_Height(0), m_SpriteIndex(0),
	m_NumOfImages(0), m_Buffer(nullptr), m_Slot(0)
{
}

Sprite::Sprite(const std::string& filename, const int images) :
	m_Filename(filename), m_Width(0), m_Height(0), m_SpriteIndex(0), 
	m_NumOfImages(images), m_Buffer(nullptr), m_Slot(0)
{	
}

void Sprite::Initialize()
{
	int bitsperpixels;
	int numOfPixelComponents = 4;
	stbi_set_flip_vertically_on_load(1);
	m_Buffer = stbi_load(m_Filename.c_str(), &m_Width, &m_Height, &bitsperpixels, numOfPixelComponents);

	// Assemble separate images
	for (int spriteIdx = 0; spriteIdx < m_NumOfImages; ++spriteIdx)
	{
		m_Textures.push_back(new Texture());
	}
	for (int spriteIdx = 0; spriteIdx < m_NumOfImages; ++spriteIdx)
	{
		unsigned char* tmpBuffer = (unsigned char*)malloc(numOfPixelComponents * m_Height * (m_Width / m_NumOfImages) * sizeof(unsigned char));
		for (int rowIdx = 0; rowIdx < m_Height; ++rowIdx)
		{
			unsigned int inOffset = (rowIdx * m_Width * numOfPixelComponents) + (spriteIdx * m_Width * numOfPixelComponents / m_NumOfImages);
			unsigned int outOffset = rowIdx * m_Width * numOfPixelComponents / m_NumOfImages;
			size_t size = m_Width * numOfPixelComponents / m_NumOfImages;
			std::cout << "Sprite: " << spriteIdx << ", rowIdx: " << rowIdx << ", In: " << inOffset << ", Out: " << outOffset << ", Size: " << size << std::endl;
			memcpy((void*)(tmpBuffer + outOffset), (void*)(m_Buffer + inOffset), size);
		}

		m_Textures[spriteIdx]->Initialize(tmpBuffer, m_Width / m_NumOfImages, m_Height, bitsperpixels);
	}
}

Sprite::~Sprite()
{
	for (int spriteIdx = 0; spriteIdx < m_Textures.size(); ++spriteIdx)
	{
		m_Textures[spriteIdx]->Free();
	}
}

void Sprite::NextImage()
{
	++m_SpriteIndex;
	if (m_SpriteIndex == m_NumOfImages)
	{
		m_SpriteIndex = 0;
	}
}

void Sprite::Bind(Shader& shader)
{
	shader.Bind();
	shader.SetUniform1i("u_Texture", m_SpriteIndex);
	m_Textures[m_SpriteIndex]->Bind(m_SpriteIndex);
}

void Sprite::Unbind()
{
	m_Textures[m_SpriteIndex]->Unbind();
}


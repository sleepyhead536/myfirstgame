#pragma once
#include <string>
#include "Core.h"
#include "Event.h"

class Window; 

struct WindowData
{
	std::string title;
	unsigned int width;
	unsigned int height;
	EventCallbackFunct callback;
	Window* sharedWindow;
};

class Window
{
public:
	Window();
	Window(WindowData data);
	~Window();
	void Initialize();
	void SetEventCallback(const EventCallbackFunct& callback);	
	void Update();
	void Finalize();
	bool IsWindowOpen();
	GLFWwindow* GetWindow() {return m_Window;};
	void MakeContextCurrent();
	void FlushCurrentContext();
private:
	WindowData m_Data;
	GLFWwindow* m_Window;
};
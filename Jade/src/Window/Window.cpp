#include "Core.h"
#include "Window.h"
#include "Logging.h"
#include "ApplicationEvent.h"
#include "KeyEvent.h"
#include "MouseEvent.h"


Window::Window()
	: m_Data{"Jade", 600, 600, NULL}
{
	Initialize();
}

Window::Window(WindowData data)
	: m_Data(data)
{
	Initialize();
}

Window::~Window()
{

}

void Window::Initialize()
{
	ASSERT(glfwInit());

	if (m_Data.sharedWindow == NULL)
	{
    	m_Window = glfwCreateWindow((int)m_Data.width, (int)m_Data.height, m_Data.title.c_str(), NULL, NULL);
	}
	else
	{
    	m_Window = glfwCreateWindow((int)m_Data.width, (int)m_Data.height, m_Data.title.c_str(), NULL, m_Data.sharedWindow->GetWindow());
	}

	if (!m_Window)
	{
		glfwTerminate();
		ASSERT(false);
	}

	MakeContextCurrent();
	glfwSetWindowUserPointer(m_Window, &m_Data);
	glfwSwapInterval(1);

	// Switching to modern Open GL
	if (glewInit() != GLEW_OK)
		ASSERT(false);

	glfwSetWindowSizeCallback(m_Window, [](GLFWwindow* window, int width, int height)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		data.width = width;
		data.height = height;

		WindowResizeEvent event(width, height);
		data.callback(event);
	});

	glfwSetWindowCloseCallback(m_Window, [](GLFWwindow* window)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

		WindowCloseEvent event;
		data.callback(event);
	});

	glfwSetKeyCallback(m_Window, [](GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		
		switch (action)
		{
			case GLFW_PRESS:
			{
				KeyPressedEvent event(key, 0);
				data.callback(event);
				break;
			}
			case GLFW_RELEASE:
			{
				KeyReleasedEvent event(key);
				data.callback(event);
				break;
			}
			case GLFW_REPEAT:
			{
				KeyPressedEvent event(key, 1);
				data.callback(event);
				break;
			}
		}
	});

	glfwSetMouseButtonCallback(m_Window, [](GLFWwindow* window, int button, int action, int mods)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

		switch (action)
		{
			case GLFW_PRESS:
			{
				MouseButtonPressedEvent event(button);
				data.callback(event);
				break;
			}
			case GLFW_RELEASE:
			{
				MouseButtonReleasedEvent event(button);
				data.callback(event);
				break;
			}
		}
	});

	glfwSetScrollCallback(m_Window, [](GLFWwindow* window, double x_offset, double y_offset)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		MouseScrolledEvent event((float)x_offset, (float)y_offset);
		data.callback(event);
	});

	glfwSetCursorPosCallback(m_Window, [](GLFWwindow* window, double xPos, double yPos)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		MouseMovedEvent event((float)xPos, (float)yPos);
		data.callback(event);
	});
}

void Window::Update()
{
	MakeContextCurrent();
	glfwSwapBuffers(m_Window);
	glfwPollEvents();
}

void Window::Finalize()
{
	glfwDestroyWindow(m_Window);
}

void Window::SetEventCallback(const EventCallbackFunct& callback)
{
	m_Data.callback = callback;
}

bool Window::IsWindowOpen()
{
	return !glfwWindowShouldClose(m_Window);
}

void Window::FlushCurrentContext()
{
	glfwMakeContextCurrent(nullptr);
}

void Window::MakeContextCurrent()
{
	glfwMakeContextCurrent(m_Window);
}
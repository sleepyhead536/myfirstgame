#pragma once 
#include "Mediator.h"
#include <functional>
class Event;

static unsigned int EntityClientCounter = 0;

class EntityClient : public MediatorClient
{
public:
    EntityClient();
    ~EntityClient();
    virtual MediatorClientType GetType() { return MediatorClientType::Entity; }
    void Initialize();
    void Update();
    void OnEvent(Event& event);

    void SetInitializeCallback(std::function<void()> callback) { m_InitializeCallback = callback; }
    void SetUpdateCallback(std::function<void()> callback) { m_UpdateCallback = callback; }
    void SetOnEventCallback(std::function<void(Event&)> callback) { m_OnEventCallback = callback; }

    unsigned int GetID() { return m_ID; }

private: 
    unsigned int m_ID;
    std::function<void()> m_InitializeCallback;
    std::function<void()> m_UpdateCallback;
    std::function<void(Event&)> m_OnEventCallback;
};

class EntityMediator : public Mediator
{
public:
    EntityMediator();
    ~EntityMediator();

    void InitializeEntities();
    void UpdateEntities();
    void PassEventToEntities(Event& event);
    EntityClient* CastToEntityClient(MediatorClient* client);
    EntityClient* GetClientByID(unsigned int id);
    void DestroyClient(EntityClient* client);
    void DestroyClient(unsigned int id);
};
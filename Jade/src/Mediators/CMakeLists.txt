set (target Mediators)

set (sources 
    EntityClient.cpp
    EntityMediator.cpp
)

set (headers
    Mediator.h
    EntityMediator.h
)

add_library(${target} ${sources} ${headers})

target_include_directories(Mediators
    PRIVATE ${CMAKE_SOURCE_DIR}/src/Events
    PRIVATE ${CMAKE_SOURCE_DIR}/src/Infrastructure
)


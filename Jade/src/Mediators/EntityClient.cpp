#include "EntityMediator.h"
#include "Event.h"

EntityClient::EntityClient() : MediatorClient(MediatorClientType::Entity),
    m_InitializeCallback(nullptr), m_UpdateCallback(nullptr), m_OnEventCallback(nullptr),
    m_ID(++EntityClientCounter)
{

}

EntityClient::~EntityClient()
{
    m_InitializeCallback = nullptr;    
    m_UpdateCallback = nullptr;    
    m_OnEventCallback = nullptr;    
}

void EntityClient::Initialize()
{
    if (m_InitializeCallback != nullptr)    
    {
        m_InitializeCallback();
    }
}

void EntityClient::Update()
{
    if (m_UpdateCallback != nullptr)    
    {
        m_UpdateCallback();
    }
}

void EntityClient::OnEvent(Event& event)
{
    if (m_OnEventCallback != nullptr)    
    {
        m_OnEventCallback(event);
    }
}
#include "EntityMediator.h"


EntityMediator::EntityMediator()
{

}
EntityMediator::~EntityMediator()
{

}
void EntityMediator::InitializeEntities()
{
	for (Clients::iterator clientIter = m_Clients.begin(); clientIter != m_Clients.end(); ++clientIter)
	{
        EntityClient* entityClient = CastToEntityClient(*clientIter);
        if (entityClient != nullptr)
        {
            entityClient->Initialize();
        }
	}
}
void EntityMediator::UpdateEntities()
{
	for (Clients::iterator clientIter = m_Clients.begin(); clientIter != m_Clients.end(); ++clientIter)
	{
        EntityClient* entityClient = CastToEntityClient(*clientIter);
        if (entityClient != nullptr)
        {
            entityClient->Update();
        }
	}
}
void EntityMediator::PassEventToEntities(Event& event)
{
	for (Clients::iterator clientIter = m_Clients.begin(); clientIter != m_Clients.end(); ++clientIter)
	{
        EntityClient* entityClient = CastToEntityClient(*clientIter);
        if (entityClient != nullptr)
        {
            entityClient->OnEvent(event);
        }
	}
}

EntityClient* EntityMediator::CastToEntityClient(MediatorClient* client)
{
    if (client->GetType() == MediatorClientType::Entity)
    {
        return (EntityClient*) client;
    }
    else 
    {
        return nullptr;
    }
}

EntityClient* EntityMediator::GetClientByID(unsigned int id)
{
 	for (Clients::iterator clientIter = m_Clients.begin(); clientIter != m_Clients.end(); ++clientIter)
	{
        EntityClient* entityClient = CastToEntityClient(*clientIter);
        if (entityClient->GetID() == id)
        {
            return entityClient;
        }
	}   
}

void EntityMediator::DestroyClient(EntityClient* client)
{
  	for (Clients::iterator clientIter = m_Clients.begin(); clientIter != m_Clients.end(); ++clientIter)
	{
        EntityClient* entityClient = CastToEntityClient(*clientIter);
        if (entityClient->GetID() == client->GetID())
        {
            m_Clients.erase(clientIter);
            free(entityClient);
            break;
        }
	}    
}

void EntityMediator::DestroyClient(unsigned int id)
{
    DestroyClient(GetClientByID(id));
}
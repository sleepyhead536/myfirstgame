#pragma once 
#include <vector>
#include "Logging.h"

enum class MediatorClientType
{
    None = 0,
    Application, Entity
};

class MediatorClient
{
public:
    MediatorClient(MediatorClientType type) : m_Type(type) {}
    ~MediatorClient(){};
    virtual MediatorClientType GetType() { return m_Type; }

private:
    MediatorClientType m_Type;
};

class Mediator 
{
public:
    Mediator(){};
    ~Mediator(){};
    void Free() 
    {
        for (Clients::iterator clients = m_Clients.begin(); clients < m_Clients.end(); ++clients)
        {
            free(*clients);
        }
    }

    template <typename T>
    void RegisterClient(T& client)
    {
        if (MediatorClient* baseClient = dynamic_cast<MediatorClient*>(&client))
        {
            m_Clients.push_back(&client);
        }
        else
        {
            ASSERT(false);
        }
        
    }
    typedef std::vector<MediatorClient*> Clients;

protected:
    Clients m_Clients;
};
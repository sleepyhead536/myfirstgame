#pragma once
#include <sstream>
#include "Event.h"

class CollisionEvent : public Event
{
public:
	CollisionEvent(unsigned int entityOne, unsigned int entityTwo) 
      : m_EntityOneID(entityOne), m_EntityTwoID(entityTwo) {}
	virtual int GetCategoryFlags() const override { return EventCategoryCustom; }

	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "CollisionPressedEvent: between " << m_EntityOneID << " and " << m_EntityTwoID;
		return ss.str();
	}

	static EventType GetStaticType() { return EventType::Collision; }
	virtual EventType GetEventType() const override { return GetStaticType(); }
	virtual const char* GetName() const override { return "Collision"; }

    unsigned int GetEntityOneID() { return m_EntityOneID; };
    unsigned int GetEntityTwoID() { return m_EntityTwoID; };

protected:
	unsigned int m_EntityOneID;
	unsigned int m_EntityTwoID;
};

#pragma once
#include <string>
#include <ostream>
#include <functional>

enum class EventType
{
	None = 0,
	WindowClose, WindowResize,
	KeyPressed, KeyReleased,
	MouseButtonPressed, MouseButtonReleased, MouseScrolled, MouseMoved,
	Collision, PuzzleSolved
};

#define BIT(x) (1<<x)
enum EventCategory
{
	None = 0,
	EventCategoryApplication	= BIT(0),
	EventCategoryInput			= BIT(1),
	EventCategoryKeyboard		= BIT(2),
	EventCategoryMouse			= BIT(3),
	EventCategoryMouseButton	= BIT(4),
	EventCategoryCustom     	= BIT(5)
};

class Event
{
	friend class EventDispatcher;
public:
	virtual EventType GetEventType() const = 0;
	virtual const char* GetName() const = 0;
	virtual int GetCategoryFlags() const = 0;
	virtual std::string ToString() const { return GetName(); }

	inline bool IsInCategory(EventCategory category)
	{
		return GetCategoryFlags() & category;
	}
protected:
	bool m_Handled = false;
};

using EventCallbackFunct = std::function<bool(Event&)>;

class EventDispatcher
{
public:
	EventDispatcher(Event& event)
		: m_Event(event) {}

	template<typename T>
	bool Dispatch(const EventCallbackFunct& funct)
	{
		if (m_Event.GetEventType() == T::GetStaticType())
		{
			m_Event.m_Handled = funct(*(T*)&m_Event);
		}
	}

private:
	Event& m_Event;
};

inline std::ostream& operator<<(std::ostream& os, const Event& e)
{
	return os << e.ToString();
}

#pragma once
#include <sstream>
#include "Event.h"

class PuzzleSolvedEvent : public Event
{
public:
	PuzzleSolvedEvent(unsigned int entity) : m_EntityID(entity) {}
	virtual int GetCategoryFlags() const override { return EventCategoryCustom; }

	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "PuzzleSolvedEvent: Entity " << m_EntityID;
		return ss.str();
	}

	static EventType GetStaticType() { return EventType::PuzzleSolved; }
	virtual EventType GetEventType() const override { return GetStaticType(); }
	virtual const char* GetName() const override { return "PuzzleSolved"; }

    unsigned int GetEntityID() { return m_EntityID; };

protected:
	unsigned int m_EntityID;
};


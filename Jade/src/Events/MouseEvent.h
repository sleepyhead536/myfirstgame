#pragma once
#include <sstream>
#include "Event.h"

class MouseMovedEvent : public Event
{
public:
	MouseMovedEvent(float x, float y)
		: m_MouseX(x), m_MouseY(y) {}

	inline float GetX() const { return m_MouseX; }
	inline float GetY() const { return m_MouseY; }

	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "MouseMovedEvent: " << m_MouseX << ", " << m_MouseY;
		return ss.str();
	}

	static EventType GetStaticType() { return EventType::MouseMoved; }
	virtual EventType GetEventType() const override { return GetStaticType(); }
	virtual const char* GetName() const override { return "MouseMoved"; }
	virtual int GetCategoryFlags() const override { return EventCategoryMouse | EventCategoryInput; }

private:
	float m_MouseX;
	float m_MouseY;
};

class MouseScrolledEvent : public Event
{
public:
	MouseScrolledEvent(float offsetX, float offsetY)
		: m_OffsetX(offsetX), m_OffsetY(offsetX) {}

	inline float GetOffsetX() const { return m_OffsetX; }
	inline float GetOffsetY() const { return m_OffsetY; }

	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "MouseScrolledEvent: " << m_OffsetX << ", " << m_OffsetY;
		return ss.str();
	}

	static EventType GetStaticType() { return EventType::MouseScrolled; }
	virtual EventType GetEventType() const override { return GetStaticType(); }
	virtual const char* GetName() const override { return "MouseScrolled"; }
	virtual int GetCategoryFlags() const override { return EventCategoryMouse | EventCategoryInput; }

private:
	float m_OffsetX;
	float m_OffsetY;
};

class MouseButtonEvent : public Event
{
public:
	inline int GetMouseButton() const { return m_Button; }

	virtual int GetCategoryFlags() const override { return EventCategoryMouse | EventCategoryInput; }

protected:
	MouseButtonEvent(int button) : m_Button(button) {}

	int m_Button;
};


class MouseButtonPressedEvent : public MouseButtonEvent
{
public:
	MouseButtonPressedEvent(int button)
		: MouseButtonEvent(button) {}

	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "MouseButtonPressedEvent: " << m_Button;
		return ss.str();
	}

	static EventType GetStaticType() { return EventType::MouseButtonPressed; }
	virtual EventType GetEventType() const override { return GetStaticType(); }
	virtual const char* GetName() const override { return "MouseButtonPressed"; }
	virtual int GetCategoryFlags() const override { return EventCategoryMouse | EventCategoryInput; }
};

class MouseButtonReleasedEvent : public MouseButtonEvent
{
public:
	MouseButtonReleasedEvent(int button)
		: MouseButtonEvent(button) {}

	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "MouseButtonReleasedEvent: " << m_Button;
		return ss.str();
	}

	static EventType GetStaticType() { return EventType::MouseButtonReleased; }
	virtual EventType GetEventType() const override { return GetStaticType(); }
	virtual const char* GetName() const override { return "MouseButtonReleased"; }
	virtual int GetCategoryFlags() const override { return EventCategoryMouse | EventCategoryInput; }
};



